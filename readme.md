# Ejemplos desarrollados en el taller del 9-sep-2020

Se muestran los archivos fuente generados durante la realización del taller y la presentación (ver carpeta Diapos).

## Otros recursos utilizados

- [Ficha programando a un humano](https://pigmonchu.bitbucket.io/resources/A4.pdf)
- [Instrucciones](https://pigmonchu.bitbucket.io/resources/Instrucciones.png)
- [Instrucciones alternativas](https://pigmonchu.bitbucket.io/resources/hausa.png)

Siguiendo el video en nuestro canal pueden utilizarse estos recursos y seguir el taller.
