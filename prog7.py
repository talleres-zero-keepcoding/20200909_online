import turtle

miTortuga = turtle.Turtle()

def poligono(num_lados):
    for i in range(num_lados):
        miTortuga.forward(50)
        miTortuga.left(360/num_lados)

def triangulo():
    for i in range(3): 
        miTortuga.forward(50)
        miTortuga.left(120)
        
def cuadrado():
    for i in range(4): 
        miTortuga.forward(50)
        miTortuga.left(90)

def pentagono():
    for i in range(5): 
        miTortuga.forward(50)
        miTortuga.left(72)
    
tipo_figura = input("Triangulo o Cuadrado (T/C/P):")

if tipo_figura == 'T':
    poligono(3)
elif tipo_figura == 'P':
    poligono(5)
else:
    poligono(4)
    
print('Acabé')






