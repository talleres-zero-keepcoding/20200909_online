import turtle

miTortuga = turtle.Turtle()

def triangulo():
    for i in range(3): 
        miTortuga.forward(50)
        miTortuga.left(120)
        
def cuadrado():
    for i in range(4): 
        miTortuga.forward(50)
        miTortuga.left(90)

def pentagono():
    for i in range(5): 
        miTortuga.forward(50)
        miTortuga.left(72)
    
tipo_figura = input("Triangulo o Cuadrado (T/C/P):")

if tipo_figura == 'T':
    triangulo()
elif tipo_figura == 'P':
    pentagono()
else:
    cuadrado()
    
print('Acabé')





